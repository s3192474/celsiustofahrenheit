package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public String convertToFahrenheit(String temp){
        Double tempInCelsius = Double.parseDouble(temp);
        return Double.toString((tempInCelsius * 1.8) + 32);
    }
}
